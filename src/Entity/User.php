<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

// * // attributes={"access_control"="is_granted('ROLE_USER')"},
// * @ApiResource(
// *     collectionOperations={
// *         "get",
// *         "post"={"access_control"="is_granted('ROLE_ADMIN')"}
// *     },
// *     itemOperations={
// *         "get",
// *         "put"={"access_control"="is_granted('ROLE_USER') and object.id == user.id"},
// *         "delete"={"access_control"="is_granted('ROLE_ADMIN')"}
// *     }
// * )

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Video", mappedBy="creator")
     * @ORM\JoinColumn(nullable=true)
     */
    private $videos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Room", mappedBy="creator")
     */
    private $rooms;

    public function __construct()
    {
        $this->videos = new ArrayCollection();
        $this->rooms = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Video[]
     */
    public function getVideos(): Collection
    {
        return $this->videos;
    }

    public function addVideo(Video $video): self
    {
        if (!$this->videos->contains($video)) {
            $this->videos[] = $video;
            $video->setCreator($this);
        }

        return $this;
    }

    public function removeVideo(Video $video): self
    {
        if ($this->videos->contains($video)) {
            $this->videos->removeElement($video);
            // set the owning side to null (unless already changed)
            if ($video->getCreator() === $this) {
                $video->setCreator(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Room[]
     */
    public function getRooms(): Collection
    {
        return $this->rooms;
    }

    public function addRoom(Room $room): self
    {
        if (!$this->rooms->contains($room)) {
            $this->rooms[] = $room;
            $room->setCreator($this);
        }

        return $this;
    }

    public function removeRoom(Room $room): self
    {
        if ($this->rooms->contains($room)) {
            $this->rooms->removeElement($room);
            // set the owning side to null (unless already changed)
            if ($room->getCreator() === $this) {
                $room->setCreator(null);
            }
        }

        return $this;
    }
}
