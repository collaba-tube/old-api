<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

// * @ApiResource(
// *     collectionOperations={
// *         "get",
// *         "post"
// *     },
// *     itemOperations={
// *         "get",
// *         "delete"={"access_control"="is_granted('ROLE_ADMIN') or is_granted('ROLE_USER') and object.creator == user"}
// *     }
// * )

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\VideoRepository")
 * @ApiFilter(SearchFilter::class, properties={"room": "exact"})
 */
class Video
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Room", inversedBy="playlist")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank
     */
    private $room;

    /**
     * @ORM\Column(type="string", length=12)
     * @Assert\NotBlank
     */
    private $vid;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="videos")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank
     */
    private $creator;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRoom(): ?Room
    {
        return $this->room;
    }

    public function setRoom(?Room $room): self
    {
        $this->room = $room;

        return $this;
    }

    public function getVid(): ?string
    {
        return $this->vid;
    }

    public function setVid(string $vid): self
    {
        $this->vid = $vid;

        return $this;
    }

    public function getCreator(): ?User
    {
        return $this->creator;
    }

    public function setCreator(?User $creator): self
    {
        $this->creator = $creator;

        return $this;
    }
}
