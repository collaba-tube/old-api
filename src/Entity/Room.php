<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

// * @ApiResource(
// *     collectionOperations={
// *         "get",
// *         "post"
// *     },
// *     itemOperations={
// *         "get",
// *         "put"={"access_control"="is_granted('ROLE_USER') and object.creator == user"},
// *         "delete"={"access_control"="is_granted('ROLE_ADMIN') or is_granted('ROLE_USER') and object.creator == user"}
// *     }
// * )

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\RoomRepository")
 */
class Room
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Video", mappedBy="room")
     */
    private $playlist;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="rooms")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank
     */
    private $creator;

    public function __construct()
    {
        $this->playlist = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Video[]
     */
    public function getPlaylist(): Collection
    {
        return $this->playlist;
    }

    public function addVideo(Video $video): self
    {
        if (!$this->playlist->contains($video)) {
            $this->playlist[] = $video;
            $video->setRoom($this);
        }

        return $this;
    }

    public function removeVideo(Video $video): self
    {
        if ($this->playlist->contains($video)) {
            $this->playlist->removeElement($video);
            // set the owning side to null (unless already changed)
            if ($video->getRoom() === $this) {
                $video->setRoom(null);
            }
        }

        return $this;
    }

    public function getCreator(): ?User
    {
        return $this->creator;
    }

    public function setCreator(?User $creator): self
    {
        $this->creator = $creator;

        return $this;
    }
}
